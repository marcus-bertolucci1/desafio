#Desafio Luiza Labs

### Pré requisitos
#### Python
Versão 2.7

#### Postgre
Versão 10.1

#### Facebook App
Criar um app no facebook que deve receber as permissões do usuário para conseguir recuperar as informações do mesmo.

**Obs:** Existe um html na home do projeto, que foi adicionado somente para facilitar um usuário a autorizar autorizar o app a pegar as devidas informações do usuário

### Realizando download da aplicação
```bash
user@host:~$ git clone https://marcus-bertolucci1@bitbucket.org/marcus-bertolucci1/desafio.git
```
### Criando e ativando o VirtualEnv
```bash
user@host:~$ virtualenv env
user@host:~$ source env/bin/activate
(env) user@host:~$
```
### Instalando as depêndencias
```bash
(env) user@host:~/desafio$ pip install -r requirements.txt
```
### Criação de arquivo .env
Copiar o arquivo .env_example alterar os devidos valores

| Parâmetro  | Descrição |
|---|---|
| SQLALCHEMY_DATABASE_URI  | URI de conexão com o Postgre |
| FACEBOOK_APP_ID  | ID do app criado no facebook |
| FACEBOOK_APP_SECRET  | Secret do app criado no facebook  |
| LOG_OUTPUT  | Onde será o output dos logs, **file**(pasta /logs) ou **stdout**  |
 

### Migrate
Este comando deleta(se existir) e cria base de dados
```bash
(env) user@host:~/desafio$ python manage.py recreate_db
```

### Executando a aplicação
#### Start
Para iniciar o servidor basta rodar o comando:
```bash
(env) user@host:~/desafio$ python manage.py runserver
```
A aplicação inicia na porta 5000

Para autorizar o app é preciso acessar http://127.0.0.1:5000 e logar com o facebook. Após isso o usuário poderá ser criado usando a api.

### Executando testes
#### Test
Para rodar os tests execute o comando:
```bash
(env) user@host:~/desafio$ python manage.py test
(env) user@host:~/desafio$ python manage.py cov
```

### API REST
#### /person
##### **POST** /api/person
Cadastra uma nova pessoa.
###### body
| Parâmetro | Descrição | Obrigatório |
| --- | --- | --- |
| facebookId | Id do usuário do facebook | Sim |

###### Retorno
| Código HTTP  | Descrição|
| --- | --- |
| 201 | Usuário criado |
| 400 | Erro para recuperar usuário do facebook |
| 409 | Usuário já existe |

##### **GET** /api/person
Retorna uma lista de pessoas.
###### querystring
| Parâmetro | Descrição | Obrigatório |
| --- | --- | --- |
| limit | Limita o número de pessoas da resposta | Não |

###### Retorno
| Código HTTP  | Descrição|
| --- | --- |
| 200 | Lista pessoas |
| 400 | Se limit for menor ou igual a 0(zero) |

```json
[
   {
      "facebookId":"123456789",
      "email":"email@mail.com",
      "name":"For Bar",
      "gender":"male"
   }
]
```
##### **DELETE** /api/person/{facebook_id}
Deleta uma pessoa.
###### url
| Parâmetro | Descrição | Obrigatório |
| --- | --- | --- |
| facebook_id | Id do usuário do facebook | Sim |

###### Retorno
| Código HTTP  | Descrição|
| --- | --- |
| 204 | Usuário excluido |
| 404 | Usuário não encontrado |

