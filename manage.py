# manage.py
import unittest
import coverage

from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from app import create_app, db
from app.models import Person

COV = coverage.coverage(
    branch=True,
    include='app/*',
    omit=[
        'app/tests/*'
    ]
)
COV.start()

app = create_app()
manager = Manager(app)
migrate = Migrate(app, db)

manager.add_command('db', MigrateCommand)


@manager.command
def test():
    tests = unittest.TestLoader().discover('app/tests', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1


@manager.command
def cov():
    tests = unittest.TestLoader().discover('app/tests')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        COV.stop()
        COV.save()
        print('Coverage Summary:')
        COV.report()
        COV.html_report()
        COV.erase()
        return 0
    return 1


@manager.command
def recreate_db():
    """Recreates a database."""
    from sqlalchemy_utils import database_exists, create_database, drop_database
    database_url = app.config['SQLALCHEMY_DATABASE_URI']
    if database_exists(database_url):
        print('Deleting database.')
        drop_database(database_url)

    if not database_exists(database_url):
        print('Creating database.')
        create_database(database_url)

    db.create_all()
    db.session.commit()


if __name__ == '__main__':
    manager.run()

