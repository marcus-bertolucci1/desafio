from datetime import datetime

from flask import current_app
import requests

from app import db


class Person(db.Model):
    __tablename__ = "person"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    facebook_id = db.Column(db.String(255), unique=True, nullable=False)
    email = db.Column(db.String(255), nullable=False)
    name = db.Column(db.String(255), nullable=False)
    gender = db.Column(db.String(6))
    created_at = db.Column(db.DateTime, nullable=False)

    def to_dict(self):
        data = {
            'id': self.id,
            'facebook_id': self.facebook_id,
            'email': self.email,
            'name': self.name,
            'gender': self.gender,
            'created_at': self.created_at.isoformat() + 'Z'
        }
        return data

    def from_dict(self, data, is_new=False):
        for field in ['facebook_id', 'email', 'name', 'gender']:
            if field in data:
                setattr(self, field, data[field])
        if is_new:
            setattr(self, 'created_at', datetime.utcnow())

    @staticmethod
    def delete_by_facebook(facebook_id):
        person = Person.query.filter_by(facebook_id=facebook_id).first()
        if not person:
            return 'NOT_FOUND'

        db.session.delete(person)
        db.session.commit()

        return

    @staticmethod
    def create_from_facebook(facebook_id):
        graph_url = ''.join([
            'https://graph.facebook.com/',  # base url
            facebook_id,  # facebook user id
            '?fields=id,name,gender,email',  # get fields
            '&access_token=',  # app authentication
            current_app.config['FACEBOOK_APP_ID'],
            '|',
            current_app.config['FACEBOOK_APP_SECRET']
        ])
        r = requests.get(graph_url)
        if r.status_code == 200:
            response_dic = r.json()
            response_dic['facebook_id'] = response_dic['id']

            exists = Person.query.filter_by(facebook_id=facebook_id).first()
            if exists:
                return 'PERSON_EXISTS'

            person = Person()
            person.from_dict(response_dic, is_new=True)
            db.session.add(person)
            db.session.commit()

            return person
        else:
            return 'FACEBOOK_ERROR'

