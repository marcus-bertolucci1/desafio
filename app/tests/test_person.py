import json
import datetime

from app.tests.base import BaseTestCase
from app.tests.utils import add_person


class TestPersonService(BaseTestCase):
    def test_get_persons(self):
        created = datetime.datetime.utcnow() + datetime.timedelta(-30)
        add_person('1', 'my_testing@email.com', 'test', 'male', created)
        add_person('2', 'another_test@email.com', 'another', 'female')
        with self.client:
            response = self.client.get('/api/person')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(data), 2)
            self.assertTrue('created_at' in data[0])
            self.assertTrue('created_at' in data[1])
            self.assertIn('1', data[0]['facebook_id'])
            self.assertIn(
                'my_testing@email.com', data[0]['email'])
            self.assertIn('2', data[1]['facebook_id'])
            self.assertIn(
                'another_test@email.com', data[1]['email'])
            self.assertNotEquals(data[0]['created_at'], data[1]['created_at'])

    def test_get_persons_limit(self):
        add_person('1', 'my_testing@email.com', 'test', 'male')
        add_person('2', 'another_test@email.com', 'another', 'female')
        with self.client:
            response = self.client.get('/api/person?limit=1')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(data), 1)

    def test_delete_person(self):
        add_person('1', 'my_testing@email.com', 'test', 'male')
        with self.client:
            response = self.client.delete('/api/person/1')
            self.assertEqual(response.status_code, 204)

    def test_delete_person_not_found(self):
        with self.client:
            response = self.client.delete('/api/person/1')
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertIn('Person not found', data['message'])
