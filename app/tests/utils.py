import datetime

from app import db
from app.models import Person


def add_person(facebook_id, email, name, gender, created_at=datetime.datetime.utcnow()):
    person = Person(
        facebook_id=facebook_id,
        email=email,
        name=name,
        gender=gender,
        created_at=created_at)
    db.session.add(person)
    db.session.commit()
    return person
