from app.models import Person
from app.tests.base import BaseTestCase
from app.tests.utils import add_person


class TestPersonModel(BaseTestCase):
    def test_add_person(self):
        person = add_person('1', 'my_testing@email.com', 'test', 'male')
        self.assertTrue(person.id)
        self.assertEqual(person.facebook_id, '1')
        self.assertEqual(person.email, 'my_testing@email.com')
        self.assertEqual(person.name, 'test')
        self.assertEqual(person.gender, 'male')
        self.assertTrue(person.created_at)

    def test_delete_person(self):
        add_person('1', 'my_testing@email.com', 'test', 'male')
        result = Person.delete_by_facebook('1')
        self.assertIsNone(result)
        exists = Person.query.filter_by(facebook_id='1').first()
        self.assertIsNone(exists)

    def test_delete_person_not_found(self):
        result = Person.delete_by_facebook('1')
        self.assertEqual(result, 'NOT_FOUND')
