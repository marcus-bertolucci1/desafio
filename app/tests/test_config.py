import os
import unittest

from flask import current_app
from flask_testing import TestCase

from app import create_app

app = create_app()


class TestDevelopmentConfig(TestCase):
    def create_app(self):
        app.config.from_object('config.DevelopmentConfig')
        return app

    def test_app_is_development(self):
        self.assertTrue(app.config['DEBUG'] is True)
        self.assertTrue(
            app.config['SQLALCHEMY_DATABASE_URI'] ==
            os.environ.get('SQLALCHEMY_DATABASE_URI')
        )
        self.assertTrue(app.config['LOG_OUTPUT'] in ['stdout', 'file'])
        self.assertTrue(app.config['FACEBOOK_APP_ID'] == os.environ.get('FACEBOOK_APP_ID'))
        self.assertTrue(app.config['FACEBOOK_APP_SECRET'] == os.environ.get('FACEBOOK_APP_SECRET'))

        self.assertFalse(current_app is None)


class TestTestingConfig(TestCase):
    def create_app(self):
        app.config.from_object('config.TestingConfig')
        return app

    def test_app_is_testing(self):
        self.assertTrue(app.config['DEBUG'] is True)
        self.assertTrue(app.config['TESTING'] is True)
        self.assertTrue(
            app.config['SQLALCHEMY_DATABASE_URI'] ==
            os.environ.get('SQLALCHEMY_DATABASE_URI')
        )
        self.assertTrue(app.config['LOG_OUTPUT'] in ['stdout', 'file'])
        self.assertTrue(app.config['FACEBOOK_APP_ID'] == os.environ.get('FACEBOOK_APP_ID'))
        self.assertTrue(app.config['FACEBOOK_APP_SECRET'] == os.environ.get('FACEBOOK_APP_SECRET'))


if __name__ == '__main__':
    unittest.main()
