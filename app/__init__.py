import os
import logging
import traceback

from flask import Flask, render_template, request

from logging.handlers import RotatingFileHandler
from flask_request_id import RequestID
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import DevelopmentConfig

db = SQLAlchemy()
migrate = Migrate()


def create_app(cfg=DevelopmentConfig):
    app = Flask(__name__)
    app.config.from_object(cfg)

    RequestID(app)
    db.init_app(app)
    migrate.init_app(app, db)

    from app.api import bp as api_bp
    app.register_blueprint(api_bp, url_prefix='/api')

    if not app.debug and not app.testing:
        if app.config['LOG_OUTPUT'] == "stdout":
            stream_handler = logging.StreamHandler()
            stream_handler.setLevel(logging.INFO)
            app.logger.addHandler(stream_handler)
        elif app.config['LOG_OUTPUT'] == "file":
            if not os.path.exists('logs'):
                os.mkdir('logs')

            file_handler = RotatingFileHandler('logs/facebook-service.log',
                                               maxBytes=10240, backupCount=10)
            file_handler.setFormatter(logging.Formatter(
                '%(asctime)s %(levelname)s: %(message)s '
                '[in %(pathname)s:%(lineno)d]'))
            file_handler.setLevel(logging.INFO)
            app.logger.addHandler(file_handler)

        app.logger.setLevel(logging.INFO)
        app.logger.info('Facebook service startup')

    if not app.config['TESTING']:
        @app.before_request
        def before_request():
            app.logger.info('request_id: %s remote_addr: %s method: %s scheme: %s path: %s context: REQUEST',
                            request.environ.get("FLASK_REQUEST_ID"), request.remote_addr, request.method, request.scheme,
                            request.full_path)

        @app.after_request
        def after_request(response):
            if response.status_code < 500:
                app.logger.info('request_id: %s remote_addr: %s method: %s scheme: %s path: %s status: %s context: RESPONSE',
                                request.environ.get("FLASK_REQUEST_ID"), request.remote_addr, request.method,
                                request.scheme, request.full_path, response.status)
            else:
                tb = traceback.format_exc()
                app.logger.error('%s %s %s %s 5xx INTERNAL SERVER ERROR\n%s', request.remote_addr, request.method,
                                 request.scheme, request.full_path, tb)
                app.logger.info(
                    'request_id: %s remote_addr: %s method: %s scheme: %s path: %s status: %s context: RESPONSE tb: %s',
                    request.environ.get("FLASK_REQUEST_ID"), request.remote_addr, request.method,
                    request.scheme, request.full_path, response.status, tb)

            return response

        # Only for facebook app permission
        @app.route('/')
        def home():
            app_id = app.config['FACEBOOK_APP_ID']
            return render_template('index.html', app_id=app_id)

    return app
