from flask import jsonify, request

from app.api import bp
from app.models import Person
from app.api.errors import bad_request, error_response


@bp.route('/person', methods=['GET'])
def get_persons():
    limit = request.args.get('limit', None, int)
    if limit and not limit > 0:
        return bad_request('limit greater than zero')

    persons = Person.query.limit(limit).all()
    data = [item.to_dict() for item in persons]
    return jsonify(data)


@bp.route('/person/<facebook_id>', methods=['DELETE'])
def delete_person(facebook_id):
    result = Person.delete_by_facebook(facebook_id)

    if result == 'NOT_FOUND':
        return error_response(404, 'Person not found')

    return jsonify(), 204


@bp.route('/person', methods=['POST'])
def create_person():
    data = request.get_json() or {}
    if 'facebook_id' not in data:
        return bad_request('facebook_id is required')

    result = Person.create_from_facebook(data['facebook_id'])
    if result == 'PERSON_EXISTS':
        return error_response(409, 'Person already exists')
    elif result == 'FACEBOOK_ERROR':
        return error_response(400, 'Facebook user error')

    return jsonify(), 201
